import React from 'react'
import { Typograpy, Button, Divider} from '@material-ui/core'
import {Elements, cardElement, ElementConsumer} from '@stripe/react-stripe-js'
import {loadStripe} from '@stripe/stripe-js'

import Review from './Review'

const PaymentForm = ({checkoutToken}) => {
    return (
        <>
            <Review checkoutToken={checkoutToken}/>
        </>
    )
}

export default PaymentForm
